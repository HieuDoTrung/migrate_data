#include "history_database_operation.hpp"

#include <mongocxx/instance.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/array/view.hpp>
#include <bsoncxx/document/view.hpp>

#include <mongocxx/exception/bulk_write_exception.hpp>

#include <coda/logger.h>

#include <string>
#include <fstream>

static const std::string HISTORY_DATABASE = "historyData";

static const std::string CREDENTIALS_PATH = "/etc/coccoc/browser-sync-service/prod_credentials.env";
static const std::string MONGO_CLUSTER_ADDRESS_NUM_1 = "db3v.itim.vn";
static const std::string MONGO_CLUSTER_ADDRESS_NUM_2 = "db4v.itim.vn";
static const std::string MONGO_USERNAME = "sync_service";

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_document;

HistoryDatabaseOperation* HistoryDatabaseOperation::m_db_connection_instance = nullptr;

std::string HistoryDatabaseOperation::get_password_from_credentials()
{
	std::string text_from_credentials;

	std::ifstream is(CREDENTIALS_PATH);
	if (is.is_open())
	{
		getline(is, text_from_credentials);
		is.close();
	}

	std::size_t pos = text_from_credentials.find("=");
	return text_from_credentials.substr(pos + 1);
}

HistoryDatabaseOperation* HistoryDatabaseOperation::get_instance()
{
	if (m_db_connection_instance)
	{
		return m_db_connection_instance;
	}
	std::string password = get_password_from_credentials();
	std::string connection_string = "mongodb://sync_service:" + password + "@" + MONGO_CLUSTER_ADDRESS_NUM_1 + "," + MONGO_CLUSTER_ADDRESS_NUM_2 + "/syncData" +
											"?minPoolSize=20000&maxPoolSize=20000";
	// std::string connection_string = "mongodb://localhost:27017";
	m_db_connection_instance = new HistoryDatabaseOperation(connection_string);
	return m_db_connection_instance;
}

mongocxx::pool::entry HistoryDatabaseOperation::get_client()
{
	return m_pool.acquire();
}

mongocxx::collection HistoryDatabaseOperation::get_collection_client_belong_to(
	const std::string& client_name, mongocxx::pool::entry& client_instance)
{
	std::string collection_name = client_name.substr(0, 2);
	if ((*client_instance)[HISTORY_DATABASE].has_collection(collection_name))
	{
		return (*client_instance)[HISTORY_DATABASE][collection_name];
	}
	(*client_instance)[HISTORY_DATABASE].create_collection(collection_name);
	(*client_instance)[HISTORY_DATABASE][collection_name].create_index(
		make_document(kvp("user_id", 1), kvp("url", 1)), {});
	return (*client_instance)[HISTORY_DATABASE][collection_name];
}

HistoryDatabaseOperation::HistoryDatabaseOperation(const std::string& connection_string)
: m_uri{connection_string}
, m_pool{m_uri}
{
}
