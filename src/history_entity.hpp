#ifndef __HISTORY_ENTITY_HPP__
#define __HISTORY_ENTITY_HPP__

#include <sparsepp/spp.h>
#include <coda/logger.h>
#include <json/json.hpp>

#include <vector>

struct HistoryEntity
{
	using Visits = spp::sparse_hash_map<std::string, std::vector<int64_t>>;
	static const uint64_t maximal_visits_per_id = 280;

	HistoryEntity(const std::string& url)
	: url(url)
	{
	}
	HistoryEntity(
		const std::string& url, const Visits& visits, const std::string& title, const std::string& favicon_url)
	: url(url)
	, visits{visits}
	, title(title)
	, favicon_url(favicon_url)
	{
	}

	std::string url;
	// client_id : { usec_0, usec_1, ... }
	mutable Visits visits;
	mutable std::string title;
	mutable std::string favicon_url;

	bool operator<(const HistoryEntity& entity) const;
	bool operator<(const std::string& url) const;
};

bool operator<(const std::string& url, const HistoryEntity& entity);

#endif
