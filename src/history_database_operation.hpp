#ifndef __SYNCER_HISTORY_DATABASE_OPERATION_HPP__
#define __SYNCER_HISTORY_DATABASE_OPERATION_HPP__

#include <mongocxx/uri.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/pool.hpp>
#include <mongocxx/instance.hpp>

class HistoryDatabaseOperation
{
private:
	HistoryDatabaseOperation(const std::string &connection_string);
	static HistoryDatabaseOperation *m_db_connection_instance;
	mongocxx::instance instance{};
	mongocxx::uri m_uri;
	mongocxx::pool m_pool;

	static std::string get_password_from_credentials();

public:
	static HistoryDatabaseOperation *get_instance();
	mongocxx::pool::entry get_client();
	mongocxx::collection get_collection_client_belong_to(const std::string& client_name,
		mongocxx::pool::entry& client_instance);

	mongocxx::collection get_history_collection();
	mongocxx::bulk_write create_bulk_operations();

	HistoryDatabaseOperation(HistoryDatabaseOperation &other) = delete;
	void operator=(const HistoryDatabaseOperation &) = delete;
};

#endif
