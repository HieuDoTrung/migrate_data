#include "history_entity.hpp"
#include "entity_info.pb.h"
#include "history_database_operation.hpp"

#include <bsoncxx/builder/stream/document.hpp>
#include <mongocxx/bulk_write.hpp>

#include <fstream>
#include <stdexcept>
#include <vector>

#include <string>
#include <iostream>

#include <filesystem>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_document;

int64_t get_file_size(std::ifstream& file)
{
	std::streampos file_size;
	file.seekg(0, std::ios::end);
	file_size = file.tellg();
	if (file_size < 0)
	{
		throw std::runtime_error("Size of ifstream could not be determinated");
	}

	return file_size;
}

int64_t get_file_size(const std::string& name)
{
	try
	{
		std::ifstream file(name, std::ios::binary);
		return get_file_size(file);
	}
	catch (const std::exception& exception)
	{
		throw std::runtime_error("Size of file '" + name + "' could not be determinated");
	}
}

void file_load_block(std::ifstream& file, std::vector<char>& data, int64_t offset, int64_t block_size)
{
	file.seekg(offset, std::ios::beg);
	data.resize(block_size);
	file.read(&data[0], block_size);
	if (!file)
	{
		throw std::runtime_error("Corrupted storage. File could not be read");
	}
}

void file_load(const std::string& name, std::vector<char>& data)
{
	std::ifstream file(name, std::ios::binary);
	int64_t file_size = get_file_size(file);
	file_load_block(file, data, 0, file_size);
}

bool is_path_exist(const std::string &path)
{
  struct stat buffer;
  return (stat (path.c_str(), &buffer) == 0);
}

struct ErrorReadingFromBytes : public std::runtime_error
{
	ErrorReadingFromBytes(size_t length, size_t remaining)
	: std::runtime_error("sequency is too short for reading '" + std::to_string(length) + "' bytes, remainded '" +
						 std::to_string(remaining) + "' bytes")
	{
	}
};

template <class T>
static T get_POD_from_bytes(const std::vector<char>& bytes, size_t& offset)
{
	if (bytes.size() < offset + sizeof(T))
	{
		throw ErrorReadingFromBytes(sizeof(T), bytes.size() - offset);
	}
	T data = *reinterpret_cast<const T*>(bytes.data() + offset);
	offset += sizeof(T);
	return data;
}

template <class T>
static T get_message_from_bytes(const std::vector<char>& bytes, size_t& offset)
{
	int len = get_POD_from_bytes<int>(bytes, offset);

	if (len + offset > bytes.size()) throw ErrorReadingFromBytes(len, bytes.size() - offset);
	google::protobuf::io::CodedInputStream is(reinterpret_cast<const uint8_t*>(bytes.data() + offset), len);
	T message;
	if (!message.ParseFromCodedStream(&is))
	{
		throw std::logic_error("pb error while parsing message");
	}
	offset += len;
	return message;
}

static const int64_t us_90_days_before = (time(nullptr) - 90 * 24 * 3600) * 1000 * 1000;

namespace
{
void normalize_usecs(std::vector<int64_t>& v)
{
	std::sort(v.begin(), v.end(), std::greater<int64_t>());
	auto it = std::unique(v.begin(), v.end());
	v.resize(std::distance(v.begin(), it));
	if (v.size() > HistoryEntity::maximal_visits_per_id)
	{
		v.resize(HistoryEntity::maximal_visits_per_id);
	}
}

// This methods returns true if urls is ignored and not going to be saved
bool is_ignored_url(const std::string& url)
{
	if (url == "chrome://newtab/") return true;

	std::string url_start_with = url.substr(0, 9); // ignore if url start with "coccoc://"
	if (url_start_with == "coccoc://") return true;

	return false;
}
} // anonymous namespace

using HistorySet = std::set<HistoryEntity, std::less<>>;

static bsoncxx::array::value vector_to_bsoncxx_array(const std::vector<int64_t>& list)
{
	bsoncxx::builder::basic::array array;
	for (const auto& num : list)
	{
		array.append(num);
	}
	return array.extract();
}

HistoryEntity add_history_entry(HistorySet& history_entities,
	const std::string& url,
	const HistoryEntity::Visits& visits,
	const std::string& title,
	const std::string& favicon_url)
{
	HistoryEntity historyEntityStorage = HistoryEntity(url);
	historyEntityStorage.visits = visits;

	auto history_ptr = history_entities.insert(HistoryEntity(url));
	auto& history_entity = history_ptr.first;

	if (history_ptr.second == 0)
	{
		// With the old record, we just need to store data if it missing or needs to update
		if (!title.empty() && history_entity->title != title) historyEntityStorage.title = title;
		if (!favicon_url.empty() && history_entity->favicon_url != favicon_url)
		{
			historyEntityStorage.favicon_url = favicon_url;
		}
	}
	else
	{
		// With the new record, we need to store all information to the file
		historyEntityStorage.title = title;
		historyEntityStorage.favicon_url = favicon_url;
	}

	if (!title.empty()) history_entity->title = title;
	if (!favicon_url.empty()) history_entity->favicon_url = favicon_url;

	for (const auto& visit : visits)
	{
		auto visit_in_base = history_entity->visits.insert(visit);
		auto& usecs = visit_in_base.first->second;
		if (!visit_in_base.second)
		{
			for (auto usec : visit.second)
			{
				usecs.push_back(usec);
			}
		}
		normalize_usecs(usecs);
	}

	return historyEntityStorage;
}

bool file_exists(const std::string& name)
{
	struct stat oStat;
	return stat(name.c_str(), &oStat) != -1;
}

void write_on_file(const std::string& log, const std::string& file_path)
{
	std::ofstream log_file;
	log_file.open(file_path, std::fstream::app);
	log_file << log;
	log_file.close();
}

int main(int argc, char** argv)
{
	// Usage:
	//	- Need to provide 1 arguments:
	// 		+ First argument: path to folder which contains all data of users. example: /mnt/ssd/browser-sync/
	if (argc != 2)
	{
		std::cout << "incorrect number of arguments\n";
		return -1;
	}

	const std::string path = argv[1];

	if (!is_path_exist(path))
	{
		std::cout << "path does not exist\n";
		return -1;
	}

	for (const auto & file_lvl_1 : std::filesystem::directory_iterator(path))
	{
		std::string path_to_prefix = file_lvl_1.path();
		auto first_pos = path_to_prefix.find_last_of("/");
		std::string first_level = path_to_prefix.substr(first_pos + 1);

		if (first_level > "66") // Migrate 20% of users on br-syncv1 from file system to mongo on first stage
		{
			continue;
		}

		try
		{
			for (const auto& file_lvl_2 : std::filesystem::directory_iterator(path_to_prefix))
			{
				std::string path_to_data = file_lvl_2.path();
				auto second_pos = path_to_data.find_last_of("/");
				std::string second_level = path_to_data.substr(second_pos + 1);
				std::string history_file_path = path_to_data + "/history";
				if (!is_path_exist(history_file_path))
				{
					std::string error_message = first_level + "/" + second_level + "/history is not exist";
					throw std::runtime_error(error_message);
				}

				std::vector<char> history;
				file_load(history_file_path, history);

				int read_count = 0;
				size_t offset = 0;
				HistorySet history_entities;
				std::string user_id = first_level + second_level;
				while (offset < history.size())
				{
					++read_count;
					try
					{
						auto entity = get_message_from_bytes<sync_storage::HistoryEntity>(history, offset);

						std::string title = entity.title();
						std::string favicon_url = entity.favicon_url();

						// Update history list (1/2)
						const std::string& url = entity.url();
						if (is_ignored_url(url)) continue;

						if (entity.has_clear_visits())
						{
							// Find URL which was loaded
							auto historyLoaded = history_entities.find(url);
							if (historyLoaded != history_entities.end())
							{
								// Before erase entity, make sure the latest version of title, favicon_url are not empty
								if (title.empty()) title = historyLoaded->title;
								if (favicon_url.empty()) favicon_url = historyLoaded->favicon_url;
								history_entities.erase(historyLoaded);
							}
						}

						HistoryEntity::Visits visits;
						for (const auto& visit : entity.visits())
						{
							for (const int64_t usec : visit.usec())
							{
								if (usec > us_90_days_before)
								{
									visits[visit.client_id()].push_back(usec);
								}
							}
							normalize_usecs(visits[visit.client_id()]);
						}
						if (!visits.empty())
						{
							add_history_entry(history_entities, url, visits, title, favicon_url);
						}
					}
					catch (const std::exception& e)
					{
						std::string log_corrupted = user_id + " error corrupted load history from file. Skip the rest of history file\n";
						printf("%s", log_corrupted.c_str());
						write_on_file(log_corrupted, path + "/migrate_file_system_to_database.log");
						break;
					}
				}

				// write data into database
				HistoryDatabaseOperation* instance = HistoryDatabaseOperation::get_instance();
				mongocxx::pool::entry client = instance->get_client();
				mongocxx::collection collection = instance->get_collection_client_belong_to(user_id, client);

				mongocxx::options::bulk_write bulk_write_option;
				bulk_write_option.ordered(false);
				mongocxx::bulk_write bulk_operations = collection.create_bulk_write(bulk_write_option);

				bool is_execute = false;
				for (const auto& each_history_entity : history_entities)
				{
					for (const auto& visit : each_history_entity.visits)
					{
						// Update document if existed
						// Insert new document if no result matches query
						mongocxx::model::update_one upsert_op{
							make_document(kvp("user_id", user_id), kvp("url", each_history_entity.url), kvp("device_id", visit.first)),
							make_document(
								kvp("$set",
									make_document(kvp("title", each_history_entity.title), kvp("favicon_url", each_history_entity.favicon_url))),
								kvp("$addToSet",
									make_document(
										kvp("timestamp", make_document(kvp("$each", vector_to_bsoncxx_array(visit.second)))))),
								kvp("$setOnInsert",
									make_document(
										kvp("url", each_history_entity.url), kvp("clear_visit", false), kvp("device_id", visit.first))))};
						upsert_op.upsert(true);
						is_execute = true;
						bulk_operations.append(upsert_op);
					}
				}
				if (is_execute)
				{
					auto result = bulk_operations.execute();
					std::string log_success = user_id + " successful inserted count: " + std::to_string(result->upserted_count()) + " modified count: " + std::to_string(result->modified_count()) + "\n";
					printf("%s",log_success.c_str());
					write_on_file(log_success, path + "/migrate_file_system_to_database.log");
				}
			}
		}
		catch (std::exception& e)
		{
			std::string log_error = path_to_prefix + " error on directory: " + std::string(e.what()) + "\n";
			printf("%s", log_error.c_str());
			write_on_file(log_error, path + "/migrate_file_system_to_database.log");
		}
	}
	return 0;
}
