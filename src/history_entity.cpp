#include "history_entity.hpp"

#include "entity_info.pb.h"

bool HistoryEntity::operator<(const HistoryEntity& entity) const
{
	return url < entity.url;
}

bool HistoryEntity::operator<(const std::string& other_url) const
{
	return url < other_url;
}

bool operator<(const std::string& url, const HistoryEntity& entity)
{
	return url < entity.url;
}
